import unireedsolomon as rs
import numpy as np

random_init = np.random.randint(2, size=10)
u = ""
for elem in random_init:
    u += str(elem)

print("U: ", u)

coder = rs.RSCoder(20, 10)
v = coder.encode(u)

print("V: ", repr(v))

r = "1"*3 + v[3:]

print("R: ", repr(r))

v_hat = coder.decode(r)
print("V HAT:", v_hat)
u_hat = v_hat[0]
print("U HAT: ", repr(u_hat))

if u == u_hat:
    print("Reconciled Successfully")
else:
    print("Nope")

to_ret = []
for number in u_hat:
    if number  == "1":
        to_ret.append(1)
    else:
        to_ret.append(0)
print("U HAT AS A LIST: ", to_ret)

parity = v_hat[1]

print("PARITY: ", repr(parity))

bob = "0000011010"
print("BOB: ", bob)
bob += parity

print("BOB WITH ALICE'S PARITY: ",repr(bob))
print("RECONCILED BOB: ", coder.decode(bob))

