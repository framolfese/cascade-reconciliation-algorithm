import math
import numpy as np
import random

def randomBob():
    bob = []
    for i in alice:
        j = np.random.rand()
        if j > 0.9:
            if i == 1:
                bob.append(0)
            else:
                bob.append(1)
        else:
            bob.append(i)
    return bob

def hammingDistance(a, b):
    distance = 0
    for i in range(0, len(a)):
        if a[i] != b[i]:
            distance += 1
    return distance/len(a)

def keyShuffling(current_iteration):
    #print("Bob visto nella shuffle: ", bob)
    indexes = list(range(0, 50))
    if current_iteration != 0:
        permuted_bob = []
        shuffle = list(range(0, 50))
        shuffle = random.sample(shuffle, len(shuffle))
        for i in indexes:
            permuted_bob.append(bob[shuffle[i]])
            #print("i = {}, shuffle[i] = {}, bob[shuffle[i]] = {}, permuted_bob[i] = {}".format(i, shuffle[i], bob[shuffle[i]], permuted_bob[i]))
    else:
        permuted_bob = bob
        shuffle = list(range(0, 50))
    return shuffle, permuted_bob

def computeNumberOfBlocks(current_iteration, hamming_distance):
    n_blocks = math.floor(0.73/hamming_distance)
    if current_iteration != 0:
        if current_iteration == 2:
            n_blocks = 2*n_blocks
        elif current_iteration == 3:
            n_blocks = 4*n_blocks
        else:
            n_blocks = 8*n_blocks
    return n_blocks

def computeParity(block):
    count = 0
    for elem in block:
        if elem == 1:
            count += 1
    if count % 2 == 0:
        return 0
    else:
        return 1

def askParity(index_list):
    count = 0
    for i in index_list:
        if alice[i] == 1:
            count += 1
    if count % 2 == 0:
        return 0
    else:
        return 1

def Binary(block, index_list):
    if(len(block) == 1 and len(index_list) == 1):
        if bob[index_list[0]] == 1:
            bob[index_list[0]] = 0
        else:
            bob[index_list[0]] = 1
    else:
        #Divide the current permuted block into two sub-blocks
        left_sub_block = block[0:math.ceil(len(block)/2)]
        #print("LEFT SUB BLOCK: ", left_sub_block)
        left_index_list = index_list[0:math.ceil(len(block)/2)]
        #print("LEFT INDEX LIST: ", left_index_list)
        #Computing the indexes of the sub-blocks in the permuted order
        right_sub_block = block[math.ceil(len(block)/2):len(block)]
        #print("RIGHT SUB BLOCK: ", right_sub_block)
        right_index_list = index_list[math.ceil(len(block)/2):len(block)]
        #print("RIGHT INDEX LIST: ", right_index_list)
        #Computing the error parity
        current_parity = computeParity(left_sub_block)
        correct_parity = askParity(index_list[0:math.ceil(len(block)/2)])
        error_parity = current_parity == correct_parity
        #print("ERROR PARITY: ", error_parity)
        #If the error parity is Odd, then we recursively call Binary on the Odd parity sub-block
        #If the size of the Odd parity sub-block is 1, we just flip the corresponding bit in Bob to correct the error
        if error_parity == False:
            Binary(left_sub_block, left_index_list)
        else:
            Binary(right_sub_block, right_index_list)

def cascadeProtocol():
    #n_iterations = 4
    print("INIZIO CASCADE")
    # for i in range(0, n_iterations):
    for i in range(2):
        #Create a new permutation at each iteration but the first, and permute Bob under that permutation
        shuffle, permuted_bob = keyShuffling(i)
        #print("Shuffle: ",shuffle)
        #print("Permuted_bob: ",permuted_bob)
        #Compute the number of blocks following the original Cascade formula
        n_blocks = computeNumberOfBlocks(i, hamming_distance)
        #print("N_blocks: ", n_blocks)
        #Creating a list in which all the block will be stored
        blocks = []
        n_elem_in_each_block = math.floor(len(permuted_bob)/n_blocks)
        #print("N_elem_in_each_block: ", n_elem_in_each_block)
        index = 0
        if n_elem_in_each_block != 0:
            for j in range(0, n_blocks):
                if j == n_blocks-1:
                    current_block = permuted_bob[index:len(permuted_bob)]
                    blocks.append([current_block, shuffle[index:len(permuted_bob)]])
                    index = len(permuted_bob)-1
                else:
                    current_block = permuted_bob[index:index + n_elem_in_each_block]
                    blocks.append([current_block, shuffle[index:index + n_elem_in_each_block]])
                    index = index + n_elem_in_each_block
        else:
            blocks.append([permuted_bob, shuffle])
        #print("Blocks of the interation {}: {}".format(i, blocks))
        for elem in blocks:
            block = elem[0]
            index_list = elem[1]
            current_parity = computeParity(block)
            correct_parity = askParity(index_list)
            error_parity = current_parity == correct_parity
            if error_parity == False:
                #print("Odd parity found in block {}".format(elem))
                #print("Calling Binary function on block {}".format(elem))
                Binary(block, index_list)
                #print("New Bob: ", bob)
                print("New Hamming Distance: ", hammingDistance(alice, bob))
    return bob




#Function to initialize the random key
random_init = np.random.randint(2, size=50)

#Generating Alice, aka y_a
alice = []
for elem in random_init:
    alice.append(elem)
#print("Alice: ", alice)

#Generating Bob, aka y_b
bob = randomBob()
#print("Bob: ", bob)

#Computing hamming distance between y_a and y_b
hamming_distance = hammingDistance(alice, bob)
print("Hamming Distance: ",hamming_distance)

reconciled_bob = cascadeProtocol()
#print("Reconciled Bob: ", reconciled_bob)
print("New Hamming Distance: ", hammingDistance(alice, reconciled_bob))




