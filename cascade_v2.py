import math
import numpy as np
from matplotlib import pyplot as plt 
import random

def randomBob(alice, ber):
    bob = []
    for i in alice:
        j = np.random.rand()
        if j > ber:
            if i == 1:
                bob.append(0)
            else:
                bob.append(1)
        else:
            bob.append(i)
    return bob

def hammingDistance(a, b):
    distance = 0
    for i in range(0, len(a)):
        if a[i] != b[i]:
            distance += 1
    return distance/len(a)

def createNewShuffles(num_shuffles):
    shuffles_list = []
    for i in range(num_shuffles):
        if i == 0:
            shuffle = list(range(0, 100))
            shuffles_list.append(shuffle)
        else:
            shuffle = list(range(0, 100))
            shuffle = random.sample(shuffle, len(shuffle))
            shuffles_list.append(shuffle)
    return shuffles_list


def keyShuffling(current_iteration):
    #print("Bob visto nella shuffle: ", bob)
    indexes = list(range(0, 100))
    if current_iteration != 0:
        permuted_bob = []
        shuffle = shuffles_array[current_iteration]
        for i in indexes:
            permuted_bob.append(bob[shuffle[i]])
            #print("i = {}, shuffle[i] = {}, bob[shuffle[i]] = {}, permuted_bob[i] = {}".format(i, shuffle[i], bob[shuffle[i]], permuted_bob[i]))
    else:
        permuted_bob = bob
    return permuted_bob

def computeBlockSize(current_iteration, hamming_distance):
    n_elem = math.floor(0.73/hamming_distance)
    if n_elem == 0:
        n_elem = 1
    if current_iteration != 0:
        if current_iteration == 1:
            n_elem = 2*n_elem
        elif current_iteration == 2:
            n_elem = 4*n_elem
        else:
            n_elem = 8*n_elem
    return n_elem

def computeParity(block):
    count = 0
    for elem in block:
        if elem == 1:
            count += 1
    if count % 2 == 0:
        return 0
    else:
        return 1

def askParity(index_list):
    count = 0
    for i in index_list:
        if alice[i] == 1:
            count += 1
    if count % 2 == 0:
        return 0
    else:
        return 1

def Binary(block, index_list):
    if(len(block) == 1 and len(index_list) == 1):
        #print("Index_list[0]: ", index_list[0])
        return index_list[0]
    else:
        #Divide the current permuted block into two sub-blocks
        left_sub_block = block[0:math.ceil(len(block)/2)]
        #print("LEFT SUB BLOCK: ", left_sub_block)
        left_index_list = index_list[0:math.ceil(len(block)/2)]
        #print("LEFT INDEX LIST: ", left_index_list)
        #Computing the indexes of the sub-blocks in the permuted order
        right_sub_block = block[math.ceil(len(block)/2):len(block)]
        #print("RIGHT SUB BLOCK: ", right_sub_block)
        right_index_list = index_list[math.ceil(len(block)/2):len(block)]
        #print("RIGHT INDEX LIST: ", right_index_list)
        #Computing the error parity
        current_parity = computeParity(left_sub_block)
        correct_parity = askParity(index_list[0:math.ceil(len(block)/2)])
        error_parity = current_parity == correct_parity
        #print("ERROR PARITY: ", error_parity)
        #If the error parity is Odd, then we recursively call Binary on the Odd parity sub-block
        #If the size of the Odd parity sub-block is 1, we just flip the corresponding bit in Bob to correct the error
        if error_parity == False:
            index = Binary(left_sub_block, left_index_list)
        else:
            index = Binary(right_sub_block, right_index_list)
    return index

def cascadeProtocol(current_iteration):
    if current_iteration >= 4:
        return
    else:
        #Create a new permutation at each iteration but the first, and permute Bob under that permutation
        #print("ITERATION: ", current_iteration)
        changes_applied = False
        shuffle = shuffles_array[current_iteration]
        permuted_bob = keyShuffling(current_iteration)
        #print("Shuffle: ",shuffle)
        #print("Permuted_bob: ",permuted_bob)
        #Compute the number of blocks following the original Cascade formula
        n_elem_in_each_block = computeBlockSize(current_iteration, hamming_distance)
        #print("N_elem_in_each_block: ", n_elem_in_each_block)
        #Creating a list in which all the block will be stored
        blocks = []
        n_blocks = math.ceil(len(permuted_bob)/n_elem_in_each_block)
        #print("N_blocks: ", n_blocks)
        index = 0
        if n_blocks != 0:
            for j in range(0, n_blocks):
                if j == n_blocks-1:
                    current_block = permuted_bob[index:len(permuted_bob)]
                    blocks.append([current_block, shuffle[index:len(permuted_bob)]])
                    index = len(permuted_bob)-1
                else:
                    current_block = permuted_bob[index:index + n_elem_in_each_block]
                    blocks.append([current_block, shuffle[index:index + n_elem_in_each_block]])
                    index = index + n_elem_in_each_block
        else:
            blocks.append([permuted_bob, shuffle])
        #print("Blocks of the interation {}: {}".format(current_iteration, blocks))
        for elem in blocks:
            block = elem[0]
            index_list = elem[1]
            current_parity = computeParity(block)
            correct_parity = askParity(index_list)
            error_parity = current_parity == correct_parity
            if error_parity == False:
                #print("Odd parity found in block {}".format(elem))
                #print("Calling Binary function on block {}".format(elem))
                changes_applied = True
                bit_flip_index = Binary(block, index_list)
                #print("Bit flip: ", bit_flip_index)
                if bob[bit_flip_index] == 1:
                    bob[bit_flip_index] = 0
                else:
                    bob[bit_flip_index] = 1
                #print("New Bob: ", bob)
                #print("New Hamming Distance: ", hammingDistance(alice, bob))
        if changes_applied == True and current_iteration > 0:
            cascadeProtocol(current_iteration-1)
        else:
            cascadeProtocol(current_iteration+1)
            



to_plot = []
avg_hamming_distance = 0
new_avg_hamming_distance = 0
ber = 0.9
for i in range(18):
    for j in range(1000):
        #Function to initialize the random key
        random_init = np.random.randint(2, size=100)

        #Generating Alice, aka y_a
        alice = []
        for elem in random_init:
            alice.append(elem)
        #print("Alice: ", alice)

        #Generating Bob, aka y_b
        bob = randomBob(alice, ber)
        #To overcome the division by zero problem when computing the block size using the hamming distance
        while(bob == alice):
            bob = randomBob(alice, ber)
        #print("Bob: ", bob)

        #Computing hamming distance between y_a and y_b
        hamming_distance = hammingDistance(alice, bob)
        #print("Hamming Distance: ",hamming_distance)

        #Creating the four shuffle sequences
        shuffles_array = createNewShuffles(4)

        cascadeProtocol(0)

        new_hamming_distance = hammingDistance(alice, bob)
        #print("New Hamming Distance: ", new_hamming_distance)

        avg_hamming_distance += hamming_distance
        new_avg_hamming_distance += new_hamming_distance

    avg_hamming_distance = avg_hamming_distance/1000
    new_avg_hamming_distance = new_avg_hamming_distance/1000
    distance_vector = [avg_hamming_distance, new_avg_hamming_distance]
    to_plot.append(distance_vector)
    avg_hamming_distance = 0
    new_avg_hamming_distance = 0
    ber = ber - 0.05

print(to_plot)
x = []
y = []
for elem in to_plot:
    x.append(elem[0])
    y.append(elem[1])
x = np.array(x)
y = np.array(y)

plt.title("Cascade Reconciliation Algorithm Experimentations: key length = 100, number of trials for each different value of the Hamming Distance = 1000") 
plt.xlabel("Hamming distance before the Cascade application") 
plt.ylabel("Average Hamming distance after the Cascade application") 
plt.plot(x,y)
plt.show()




