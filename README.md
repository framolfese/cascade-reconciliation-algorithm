# Cascade Reconciliation Algorithm

A simple implementation of the Cascade algorithm for key reconciliation. 
The file cascade.py offers a naive implementation with no recursion between iterations (hence it is just the main paradigm used to correct errors, without any cascading effect).
The file cascade_v2.py offer the whole recursive implementation, including the cascade effect. 
Key length and Bit error rate can be set manually modifying the files.
