import math
import numpy as np
from matplotlib import pyplot as plt 
import random
import unireedsolomon as rs

def randomBob(alice, ber):
    bob = []
    for elem in alice:
        bob.append(elem)
    hd = len(alice)*ber
    indexes = []
    while(hd > 0):
        j = random.randint(0, len(alice)-1)
        while(j in indexes):
            j = random.randint(0, len(alice)-1)
        indexes.append(j)
        if bob[j] == 1:
            bob[j] = 0
        else:
            bob[j] = 1
        hd -= 1
    return bob

def hammingDistance(a, b):
    distance = 0
    for i in range(0, len(a)):
        if a[i] != b[i]:
            distance += 1
    return distance

random_init = np.random.randint(2, size=50)

alice = []
for elem in random_init:
    alice.append(elem)
ber = 0.1
bob = randomBob(alice, ber)

print("Alice: ",alice)
print("Bob: ",bob)
print("Hamming distance: ", hammingDistance(alice, bob))

coder = rs.RSCoder(50, 40)

y_a = ""
y_b = ""
random_init = np.random.randint(2, size=40)
x_list = []
x = ""
for elem in alice:
    y_a += str(elem)
for elem in random_init:
    x_list.append(elem)
for elem in x_list:
    x += str(elem)
for elem in bob:
    y_b += str(elem)

print("X: ", x)
x_coded = coder.encode(x)

print("Y_a: ", y_a)
print("Y_b: ", y_b)
print("C(x): ", x_coded)

v = [chr(ord(a) ^ ord(b)) for a,b in zip(y_a, x_coded)]

print("V = Y_a xor C(x): ",v)

y_b_list = []
for elem in y_b:
    y_b_list.append(elem)

print("Y_b as a list: ", y_b_list)


y_b_xor_v = [chr(ord(a) ^ ord(b)) for a,b in zip(v, y_b_list)]

print("Y_b xor v: ", y_b_xor_v)

y_b_xor_v_string = ""

for elem in y_b_xor_v:
    y_b_xor_v_string += elem

x_prime = coder.decode(y_b_xor_v_string)

print("X' = D(y_b xor v): ", x_prime[0])

x_prime_coded = coder.encode(x_prime[0])

print("C(x'): ", x_prime_coded)

x_prime_coded_list = []
for elem in x_prime_coded:
    x_prime_coded_list.append(elem)

reconciled = [chr(ord(a) ^ ord(b)) for a,b in zip(v, x_prime_coded_list)]
reconciled_y_b = []
for elem in reconciled:
    reconciled_y_b.append(int(elem))

print("Reconciled Y_b = v xor C(x'): ", reconciled_y_b)
print("New Hamming Distance Between Y_a and Y_b: ", hammingDistance(alice, reconciled_y_b))







